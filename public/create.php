<?php

/**
 * Use an HTML form to create a new entry in the
 * users table.
 *
 */

require "../config.php";

require "templates/header.php"; ?>
<style>
<?php include 'css/style.css'; ?>
</style>


  <?php if(isset($_FILES['image'])){
      $errors= array();
      $file_name = $_FILES['image']['name'];
      $file_size =$_FILES['image']['size'];
      $file_tmp =$_FILES['image']['tmp_name'];
      $file_type=$_FILES['image']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
      
      $extensions= array("jpeg","jpg","png");
      
      if(in_array($file_ext,$extensions)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      }
      
      if($file_size > 2097152){
         $errors[]='File size must be excately 2 MB';
      }

      $connection = new PDO($dsn, $username, $password, $options);
      $stmt = $connection->prepare("SELECT * FROM images WHERE filename = :filename");

      $stmt->bindValue(':filename', $file_name);
      $stmt->execute();

      $result = $stmt->fetchAll();

      if(!empty($result)){
        $errors[]='The file already exists';
      }

      
      if(empty($errors)==true){
        $connection = new PDO($dsn, $username, $password, $options);
        $stmt = $connection->prepare("INSERT INTO images 
            (filename, views) VALUES (:filename, 0)");

        $stmt->bindValue(':filename', $file_name);
        $stmt->execute();

        move_uploaded_file($file_tmp,"images/".$file_name);

        echo "Success";
      

      }else{
         print_r($errors);
      }
   }
  ?>

  <h2>Upload your photo</h2>

      <form action="" method="POST" enctype="multipart/form-data">
         <input type="file" name="image" />
         <input type="submit"/>
      </form>

  <a href="index.php">Back to home</a>

<?php require "templates/footer.php"; ?>
